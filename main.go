package inbound

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/emersion/go-smtp"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

func main() {
	http.ListenAndServe("[::]:80", http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				writer.WriteHeader(400)
				fmt.Printf("%s %s\n", time.Now(), err)
				_, _ = writer.Write([]byte("Error"))
				return
			}
		}()
		catch := func(err error) {
			if err != nil {
				panic(err)
			}
		}

		data, err := ioutil.ReadAll(request.Body)
		catch(err)

		body := struct {
			ID        string `json:"id"`
			Recipient string `json:"rcpt_to"`
			Sender    string `json:"mail_from"`
			Message   []byte `json:"message"`
			Base64    bool   `json:"base64"`
			Size      int
		}{}
		catch(json.Unmarshal(data, &body))

		if body.Base64 {
			msg := make([]byte, body.Size)
			_, err := base64.StdEncoding.Decode(body.Message, msg)
			catch(err)
			body.Base64 = false
			body.Message = msg
		}

		conn, err := net.Dial("tcp", "172.19.199.2")
		catch(err)

		client, err := smtp.NewClientLMTP(conn, "mailman-core")
		catch(err)

		catch(client.Mail(body.Sender, &smtp.MailOptions{Size: body.Size, UTF8: true}))
		catch(client.Rcpt(body.Recipient))
		w, err := client.Data()
		catch(err)
		_, err = w.Write(body.Message)
		catch(err)

		_, _ = writer.Write([]byte("OK"))
	}))
}
