FROM golang:alpine
ADD go.mod /build/go.mod
WORKDIR /build
RUN go mod download
ADD main.go /build/main.go
RUN CGO_ENABLED=0 go build -a -ldflags '-s -w -extldflags "-static"' -o inbound .


FROM scratch
COPY --from=0 /build/inbound /bin/inbound
EXPOSE 80
ENTRYPOINT ["/bin/inbound"]
